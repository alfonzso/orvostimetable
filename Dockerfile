FROM alpine:latest

RUN apk add --update curl w3m && wget  https://github.com/ericchiang/pup/releases/download/v0.4.0/pup_v0.4.0_linux_amd64.zip && unzip pup_v0.4.0_linux_amd64.zip -d /usr/bin && rm pup_v0.4.0_linux_amd64.zip

# CMD curl -s https://doktor24.hu/szakteruletek/haziorvos/ | pup  'div[data-widget_type="theme-post-content.default"]' --color | w3m -dump -T 'text/html' 
CMD curl -s https://doktor24.hu/szakteruletek/haziorvosi-rendeles/ | pup  'div[class="featureContent"]' --color | w3m -dump -T 'text/html'
